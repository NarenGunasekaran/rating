var spans = document.getElementsByTagName('span');

for(var i = 0, l = spans.length; i < l; i++){
    spans[i].id = i;
}

$('span').on('click', function(e){
	let id = e.target.id;

	if (!$('.stars').find('#' + id).hasClass('checked')){
		$('.stars').find('#' + id).addClass('checked');
	}

	$('.stars').find('#' + id).prevAll().addClass('checked');
	$('.stars').find('#' + id).nextAll().removeClass('checked');
	
	let count = $('.stars').find('#' + id).prevAll().length + 1;

	$('.countVal').html(count);
});

var performCheck = function(id){
	if($('#' + id).hasClass('checked')){
		$('#' + id).removeClass('checked');
	} else {
		$('#' + id).addClass('checked');
	}
}